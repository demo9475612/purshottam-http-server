const http = require('http');
const { v4: uuidv4 } = require('uuid');

const server  = http.createServer((request,response)=>{

    if(request.method==='GET' && request.url==='/html'){
        response.writeHead(200,{'Content-Type':'text/html'});
        response.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
          </body>
        </html>`);
        response.end();
    }
    if(request.method==='GET' && request.url===('/json')){
        response.writeHead(200,{'Content-Type': 'application/json'});
        response.write(`
        {
            "slideshow":{
              "author": "Yours Truly",
              "date": "date of publication",
              "slides":[
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items":[
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
        }`);
        response.end();
    }

    if(request.method==='GET' && request.url==='/uuid'){
        const uuidValue = uuidv4();
        const uuidObject = {
            uuid : uuidValue,
        }
        console.log(uuidObject);
        const uuidString = JSON.stringify(uuidObject);
        console.log(uuidString);
        response.write(uuidString);
        response.end();
    }
    
    if(request.method==='GET' && request.url.includes('/status/')){
        const url=request.url;
        console.log(url);
        const urlArray= url.split('/');
        console.log(urlArray);
        const statusCode = parseInt(urlArray[2]);
        console.log(statusCode);
        if(isNaN(statusCode) || statusCode<100){
        response.writeHead(400, {'Content-Type': 'text/plain'});
        response.end('400 Bad Request');
        }else{
        response.writeHead(statusCode,{'Constent-Type':'text/plain'});
        response.write(`${http.STATUS_CODES[statusCode]} a response with ${statusCode} status code`);
        response.end();
        }
    }

    if(request.method==='GET' && request.url.includes('/delay/')){
        const url=request.url;
        console.log(url);
        const urlArray= url.split('/');
        console.log(urlArray);
        const delayTime = parseInt(urlArray[2]);
        console.log(delayTime);
        if(isNaN(delayTime) || delayTime<0){
            response.writeHead(400,{'Content-Type':'text/plain'});
            response.end("400 Bad Request");
        }
        else{
            setTimeout(()=>{
                 response.writeHead(200,{'Content-Type':'text/plain'});
                 console.log("OK 200");
                 response.end("OK 200");
            },delayTime*1000);
        }
    }


});
server.listen(8080);
